const { servicio } =require('../services')
function adaptador({info,color}) {
    try {
        let {statusCode,data,message} = servicio({info,color})
        return {statusCode,data,message}
        
    } catch (error) {
        console.log(error)
        return {statusCode:500,message:error.toString()}
    }
    

}

module.exports = {adaptador}